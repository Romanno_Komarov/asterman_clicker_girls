﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InputFieldGetString : MonoBehaviour {

    [SerializeField]
    private InputField inputField;

    public void GetInputFieldString()
    {
        if (inputField.text != "")
        {
            PlayerPrefs.SetString("NAME_PLAYER", inputField.text);
            SceneManager.LoadScene("CreateAvatar");
        }
    }
}
