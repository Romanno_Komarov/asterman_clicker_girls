﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CarCoinsBehavior : MonoBehaviour
{

    [SerializeField]
    private Button button;

    [SerializeField]
    private Transform carCoinsButton;

    [SerializeField]
    private float speedCar;

    void Awake()
    {
        button.onClick.AddListener(OnButtonClicked);
    }

    void OnDestroy()
    {
        button.onClick.RemoveListener(OnButtonClicked);
    }

    void OnButtonClicked()
    {
        CoffeeShop_1_Manager.Instance.GetCoinsCoffeeShop();
        CoffeeShop_1_Manager.Instance.GetCoins(2.5f);//x2.5 coins for tap car

        GameManager.Instance.counterTapsCars++;
    }

    void SaveCountersTapsCars()
    {
        PlayerPrefs.SetInt("COUNTER_TAPS_CARS", GameManager.Instance.counterTapsCars);
    }

    void MovingCarCoins()
    {
        if (carCoinsButton.transform.localPosition.x > -1500)
        {
            carCoinsButton.Translate(Vector3.left * Time.deltaTime * speedCar);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void Update()
    {
        SaveCountersTapsCars();
    }

    void FixedUpdate()
    {
        MovingCarCoins();
    }
}
