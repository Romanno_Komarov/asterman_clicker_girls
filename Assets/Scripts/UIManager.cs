﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    void Awake()
    {
        GameManager.Instance.Load();
    }

    public void OpenScene(string nameScene)
    {
        SceneManager.LoadScene(nameScene);
    }

    public void StartGame()
    {
       // if (GameManager.Instance.buyedLocalIsland_1 == 1)
       // {
       //     SceneManager.LoadScene("LoadingScreen");
       // }
       // else
       // {
            SceneManager.LoadScene("LoadingScreen"); // load coffee shop
       // }
    }

    public void OpenCanvas(GameObject canvas)
    {
        canvas.SetActive(true);
    }

    public void CloseCanvas(GameObject canvas)
    {
        canvas.SetActive(false);
    }

    public void Restart()
    {
        PlayerPrefs.DeleteAll();
    }

    public void OnApplyButtonClicked()
    {
        WardrobeManager.Instanse.Buy();
        WardrobeManager.Instanse.Save();
        SceneManager.LoadScene("CoffeeShop_1");

        //SceneManager.LoadScene("GlobalMap");//open global map
    }
}
