﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PeopleCoinsBehavior_2 : MonoBehaviour
{
    [SerializeField]
    private Button button;

    [SerializeField]
    private Text textBubble;

    [SerializeField]
    private string[] stringTextBubble;

    [SerializeField]
    private Transform peopleCoinsButton;

    [SerializeField]
    private GameObject textBubblePeople;

    [SerializeField]
    private float speedPeople;

    void Awake()
    {
        button.onClick.AddListener(OnButtonClicked);
    }

    void OnDestroy()
    {
        button.onClick.RemoveListener(OnButtonClicked);
    }

    void OnButtonClicked()
    {
        CoffeeShop_1_Manager.Instance.GetCoinsCoffeeShop();
        CoffeeShop_1_Manager.Instance.GetCoins(2f);//x2 coins for tap people
        GameManager.Instance.counterTapsPeople++;
    }

    void RandomTextBuble()
    {
        int randomNumberTextBubble = Random.Range(0, stringTextBubble.Length);

        switch (randomNumberTextBubble)
        {
            case 0:
                {
                    textBubble.text = stringTextBubble[0];
                    break;
                }
            case 1:
                {
                    textBubble.text = stringTextBubble[1];
                    break;
                }
            case 2:
                {
                    textBubble.text = stringTextBubble[2];
                    break;
                }
            case 3:
                {
                    textBubble.text = stringTextBubble[3];
                    break;
                }
            case 4:
                {
                    textBubble.text = stringTextBubble[4];
                    break;
                }
        }
    }

    void SaveCountersTapsPeople()
    {
        PlayerPrefs.SetInt("COUNTER_TAPS_PEOPLE", GameManager.Instance.counterTapsPeople);
    }

    void MovingPeopleCoins()
    {
        if (peopleCoinsButton.transform.localPosition.x > -1500)
        {
            peopleCoinsButton.Translate(Vector3.right * Time.deltaTime * speedPeople);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void Start()
    {
        RandomTextBuble();
    }

    void Update()
    {
        SaveCountersTapsPeople();
    }

    void FixedUpdate()
    {
        MovingPeopleCoins();
    }
}
