﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetCheckCoinManager : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D obj)
    {
        if (obj.tag == "CoinsCoffeeShop")
        {
            Destroy(obj.gameObject);
        }
    }
}
