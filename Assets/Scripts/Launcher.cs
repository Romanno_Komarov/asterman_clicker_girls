﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : MonoBehaviour //Save keys
{
    void OnApplicationPause(bool isPause)
    {
        if (isPause)
        {
            WardrobeManager.Instanse.Save();
            GameManager.Instance.Save();
        }
    }

   void OnApplicationQuit()
   {
       OnApplicationPause(true);
   }
}
