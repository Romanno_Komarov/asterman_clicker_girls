﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterShopManager : MonoBehaviour
{
    [SerializeField]
    private GameObject ch_1;//+speed
    [SerializeField]
    private Button buttonCh_1Coins;

    [SerializeField]
    private GameObject ch_2;//+income
    [SerializeField]
    private Button buttonCh_2Coins;

    [SerializeField]
    private Button buttonCh_1Hearts;
    [SerializeField]
    private Button buttonCh_2Hearts;

    private int isBuyingCh_1 = 0;
    private int isBuyingCh_2 = 0;

    void LoadKeys()
    {
        isBuyingCh_1 = PlayerPrefs.GetInt("IS_BUYING_CH_1");
        isBuyingCh_2 = PlayerPrefs.GetInt("IS_BUYING_CH_2");
    }

    void Start()
    {
        LoadKeys();
        CheckBuyingCharacters();
    }

    void CheckBuyingCharacters()
    {
        if (isBuyingCh_1 == 1)
        {
            ch_1.SetActive(true);
            buttonCh_1Coins.interactable = false;
            buttonCh_1Hearts.interactable = false;
        }

        if (isBuyingCh_2 == 1)
        {
            ch_2.SetActive(true);
            buttonCh_2Coins.interactable = false;
            buttonCh_2Hearts.interactable = false;
        }
    }

    public void BuyCharacter_1_Coins(int price)
    {
        if (GameManager.Instance.coins >= price)
        {
            GameManager.Instance.coins -= price;
            ch_1.SetActive(true);
            buttonCh_1Hearts.interactable = false;
            buttonCh_1Coins.interactable = false;
            CoffeeShop_1_Manager.Instance.speedLoad = CoffeeShop_1_Manager.Instance.speedLoad * (100 + 25) / 100;
            PlayerPrefs.SetInt("IS_BUYING_CH_1", 1);
        }
    }

    public void BuyCharacter_2_Coins(int price)
    {
        if (GameManager.Instance.coins >= price)
        {
            GameManager.Instance.coins -= price;
            ch_2.SetActive(true);
            buttonCh_2Coins.interactable = false;
            buttonCh_2Hearts.interactable = false;
            CoffeeShop_1_Manager.Instance.coinsShop = CoffeeShop_1_Manager.Instance.coinsShop * (100 + 30) / 100;
            PlayerPrefs.SetInt("IS_BUYING_CH_2", 1);
        }
    }

    public void BuyCharacter_1_Hearts(int price)
    {
        if (GameManager.Instance.hearts >= price)
        {
            GameManager.Instance.hearts -= price;
            ch_1.SetActive(true);
            buttonCh_1Coins.interactable = false;
            buttonCh_1Hearts.interactable = false;
            CoffeeShop_1_Manager.Instance.speedLoad = CoffeeShop_1_Manager.Instance.speedLoad * (100 + 25) / 100;
            PlayerPrefs.SetInt("IS_BUYING_CH_1", 1);
        }
    }

    public void BuyCharacter_2_Hearts(int price)
    {
        if (GameManager.Instance.hearts >= price)
        {
            GameManager.Instance.hearts -= price;
            ch_2.SetActive(true);
            buttonCh_2Coins.interactable = false;
            buttonCh_2Hearts.interactable = false;
            CoffeeShop_1_Manager.Instance.coinsShop = CoffeeShop_1_Manager.Instance.coinsShop * (100 + 30) / 100;
            PlayerPrefs.SetInt("IS_BUYING_CH_2", 1);
        }
    }
}
