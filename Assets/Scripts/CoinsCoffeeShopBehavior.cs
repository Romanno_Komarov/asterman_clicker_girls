﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsCoffeeShopBehavior : MonoBehaviour
{
    private GameObject target;

    void Awake()
    {
        target = GameObject.Find("TargetImage");
    }

    void Update()
    {
        transform.position = Vector3.Lerp(transform.position, target.transform.position, 5 * Time.deltaTime);

        if (transform.position.y >= 800)
        {
            Destroy(this.gameObject);
        }
    }
}
