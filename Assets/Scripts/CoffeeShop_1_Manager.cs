﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoffeeShop_1_Manager : MonoBehaviour
{
    public static CoffeeShop_1_Manager Instance;

    public bool isPause;

    [SerializeField]
    private Text textStagesOfLevels;

    [SerializeField]
    private Transform carCoinsButton_1;

    [SerializeField]
    private Transform carCoinsButton_2;

    [SerializeField]
    private Transform ballCoinsButton;

    [SerializeField]
    private Transform peopleCoinsButton_1;

    [SerializeField]
    private Transform peopleCoinsButton_2;

    [SerializeField]
    private Image CoffeeShop;

    [SerializeField]
    private Image[] coffeeShopsStages;

    [SerializeField]
    private Text textCoins;

    [SerializeField]
    private Text textHearts;

    [SerializeField]
    private Text nameCoffeeShopText;

    [SerializeField]
    public GameObject canvasPanelMenu;

    [SerializeField]
    public GameObject canvasPlants;

    [SerializeField]
    public GameObject canvasTables;

    [SerializeField]
    public GameObject canvasCharacter;

    [SerializeField]
    public GameObject canvasNameCoffeeShop;

    [SerializeField]
    public GameObject canvasAchiev;

    [SerializeField]
    public GameObject canvasTasks;

    [SerializeField]
    public InputField inputNameCoffeeShopText;

    [SerializeField]
    private Image m_LoadingProgress;

    [SerializeField]
    private Image loadingProgressUpdateButton;

    [SerializeField]
    private GameObject m_Coins;

    [SerializeField]
    private GameObject textBubblePeople;

    [SerializeField]
    private GameObject m_CoinsCoffeeShop;

    [SerializeField]
    private Button buttonUpdateCoffeeShop;

    [SerializeField]
    private Text textButtonUpdateCoffeeShop;

    [Header("Tutorial")]
    [SerializeField]
    private Text textTutorial;
    [SerializeField]
    private GameObject circleStageTutorial;
    [SerializeField]
    private GameObject panelTutorial;
    [SerializeField]
    private string[] textStagesTutorial;
    [SerializeField]
    private int numberOfStageTutorial = 0;

    private float loading_progress = 0;
    private int roundLoad = 0;

    private float loading_progressUpdateButton = 0;
    private int roundLoadUpdateButton = 0;

    public float coinsShop;           //save this members
    public float speedLoad;         //save this members
    public int costUpdate;          //save this members
    public int numberOfUpdate = 0;  //save this members

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        //CheckFirstEnterInGame();

        GameManager.Instance.Load();

        LoadKeysCoffeeShop();

        IncreaseMembers();

        StartCoroutine(CreateCarCoins_1(15));//time for create car in seconds
        StartCoroutine(CreateCarCoins_2(20));//time for create car in seconds
        StartCoroutine(CreateBallCoins(10));//time for create ball in seconds
        StartCoroutine(CreatePeopleCoins_1(25));//time for create people in seconds
        StartCoroutine(CreatePeopleCoins_2(20));//time for create people in seconds

        textButtonUpdateCoffeeShop.text = "Update for " + costUpdate;

        TutorialManager();
    }

    void CheckLoadingProgressUpdateButton()
    {
        if (loadingProgressUpdateButton.fillAmount <= 100)
        {
            loadingProgressUpdateButton.fillAmount = 0 + GameManager.Instance.coins / costUpdate;
        }

        if (roundLoadUpdateButton == 100)
        {
            loadingProgressUpdateButton.fillAmount = 1;
        }
    }

    void LoadKeysCoffeeShop()
    {
        coinsShop = PlayerPrefs.GetInt("COINS_SHOP");
        speedLoad = PlayerPrefs.GetFloat("SPEED_LOAD");
        costUpdate = PlayerPrefs.GetInt("COST_UPDATE");
        numberOfUpdate = PlayerPrefs.GetInt("NUMBER_OF_UPDATE");
    }

    void TutorialManager()
    {
        if (GameManager.Instance.isFirstPlaying == 0)
        {
            isPause = true;
            panelTutorial.SetActive(true);
            textTutorial.text = "Hi, " + GameManager.Instance.namePlayer + "! Now I'll introduce you to the game!";
        }
    }

    public void NextStageTutorial()
    {
        numberOfStageTutorial++;

        switch (numberOfStageTutorial)
        {
            case 1:
                {
                    textTutorial.text = textStagesTutorial[1];
                    break;
                }
            case 2:
                {
                    textTutorial.text = textStagesTutorial[2];
                    circleStageTutorial.transform.localPosition = new Vector3(200, 1050, 0);
                    break;
                }
            case 3:
                {
                    textTutorial.text = textStagesTutorial[3];
                    circleStageTutorial.SetActive(false);
                    break;
                }
            case 4:
                {
                    textTutorial.text = textStagesTutorial[4];
                    break;
                }
            case 5:
                {
                    textTutorial.text = textStagesTutorial[5];
                    break;
                }
            case 6:
                {
                    panelTutorial.SetActive(false);
                    isPause = false;
                    GameManager.Instance.isFirstPlaying = 1;
                    PlayerPrefs.SetInt("IS_FIRST_PLAYING", 1);
                    break;
                }
        }
    }

    void SaveKeysCoffeeShop()
    {
        PlayerPrefs.SetFloat("COINS_SHOP", coinsShop);
        PlayerPrefs.SetFloat("SPEED_LOAD", speedLoad);
        PlayerPrefs.SetInt("COST_UPDATE", costUpdate);
        PlayerPrefs.SetInt("NUMBER_OF_UPDATE", numberOfUpdate);
    }

    // void CheckFirstEnterInGame() //viveska - uncommended
    // {
    //     if (GameManager.Instance.isEnterNameCoffeeShop == 0)
    //     {
    //         isPause = true;
    //         canvasNameCoffeeShop.SetActive(true);
    //     }
    //     else
    //     {
    //         isPause = false;
    //         //nameCoffeeShopText.text = PlayerPrefs.GetString("NAME_COFFEE_SHOP");
    //     }
    // }

    public void ApplyNameCoffeeShop()
    {
        if (inputNameCoffeeShopText.text != "")
        {
            GameManager.Instance.nameCoffeeShop = inputNameCoffeeShopText.text;
            PlayerPrefs.SetString("NAME_COFFEE_SHOP", GameManager.Instance.nameCoffeeShop);
            PlayerPrefs.SetInt("IS_ENTER_NAME_COFFEE_SHOP", 1);
            canvasNameCoffeeShop.SetActive(false);
            isPause = false;
        }
    }

    void CheckCoinsUpdate()
    {
        if (GameManager.Instance.coins >= costUpdate)
        {
            buttonUpdateCoffeeShop.interactable = true;
        }
    }

    void IncreaseMembers()
    {
        switch (numberOfUpdate)
        {
            case 0://default 
                {
                    speedLoad = 100f;
                    coinsShop = 0.01f;
                    costUpdate = 1;
                    CoffeeShop.color = Color.blue;//refresh for stages[]
                    break;
                }
            case 1:
                {
                    speedLoad = 35f;//set speed load 
                    coinsShop = 0.05f;//getting coins
                    costUpdate = 5;//price for update next lvl
                    CoffeeShop.color = Color.white;
                    break;
                }
            case 2:
                {
                    speedLoad = 35f;
                    coinsShop = 0.05f;
                    costUpdate = 10;
                    CoffeeShop.color = Color.yellow;
                    break;
                }
            case 3:
                {
                    speedLoad = 35f;
                    coinsShop = 0.05f;
                    costUpdate = 15;
                    CoffeeShop.color = Color.green;
                    break;
                }
            case 4:
                {
                    speedLoad = 35f;
                    coinsShop = 0.1f;
                    costUpdate = 30;
                    CoffeeShop.color = Color.red;
                    break;
                }
            case 5:
                {
                    speedLoad = 17f;
                    coinsShop = 0.1f;
                    costUpdate = 35;
                    CoffeeShop.color = Color.blue;
                    break;
                }
            case 6:
                {
                    speedLoad = 17f;
                    coinsShop = 0.15f;
                    costUpdate = 60;
                    CoffeeShop.color = Color.magenta;
                    break;
                }
            case 7:
                {
                    speedLoad = 17f;
                    coinsShop = 0.15f;
                    costUpdate = 70;
                    CoffeeShop.color = Color.grey;
                    break;
                }
            case 8:
                {
                    speedLoad = 17f;
                    coinsShop = 0.2f;
                    costUpdate = 100;
                    CoffeeShop.color = Color.cyan;
                    break;
                }
            case 9:
                {
                    speedLoad = 8f;
                    coinsShop = 0.2f;
                    costUpdate = 110;
                    CoffeeShop.color = Color.yellow;
                    break;
                }
            case 10:
                {
                    speedLoad = 8f;
                    coinsShop = 0.25f;
                    costUpdate = 150;
                    CoffeeShop.color = Color.red;
                    break;
                }
            case 11:
                {
                    speedLoad = 8f;
                    coinsShop = 0.25f;
                    costUpdate = 160;
                    CoffeeShop.color = Color.blue;
                    break;
                }
            case 12:
                {
                    speedLoad = 8f;
                    coinsShop = 0.3f;
                    costUpdate = 210;
                    CoffeeShop.color = Color.white;
                    break;
                }
            case 13:
                {
                    speedLoad = 4f;
                    coinsShop = 0.3f;
                    costUpdate = 225;
                    CoffeeShop.color = Color.green;
                    break;
                }
            case 14:
                {
                    speedLoad = 4f;
                    coinsShop = 0.35f;
                    costUpdate = 280;
                    CoffeeShop.color = Color.cyan;
                    break;
                }
            case 15:
                {
                    speedLoad = 4f;
                    coinsShop = 0.35f;
                    costUpdate = 300;
                    CoffeeShop.color = Color.yellow;
                    break;
                }
            case 16:
                {
                    speedLoad = 4f;
                    coinsShop = 0.4f;
                    costUpdate = 360;
                    CoffeeShop.color = Color.blue;
                    break;
                }
            case 17:
                {
                    speedLoad = 1f;
                    coinsShop = 0.4f;
                    costUpdate = 280;
                    CoffeeShop.color = Color.green;
                    break;
                }
            case 18:
                {
                    speedLoad = 1f;
                    coinsShop = 0.45f;
                    costUpdate = 450;
                    CoffeeShop.color = Color.white;
                    break;
                }
            case 19:
                {
                    speedLoad = 1f;
                    coinsShop = 0.5f;
                    costUpdate = 750;
                    CoffeeShop.color = Color.red;
                    break;
                }
            case 20://ended lvl // Debug(WIN!)
                {
                    speedLoad = 0.5f;
                    coinsShop = 1f;
                    costUpdate = 10000;
                    CoffeeShop.color = Color.white;
                    break;
                }
        }
    }

    public void UpdateCoffeeShop()
    {
        if (GameManager.Instance.coins >= costUpdate)
        {
            GameManager.Instance.coins -= costUpdate;
            numberOfUpdate++;
            IncreaseMembers();
            buttonUpdateCoffeeShop.interactable = false;
        }
        textButtonUpdateCoffeeShop.text = "Upgrade " + costUpdate;
    }

    void StartGenerateCoins()
    {
        if (m_LoadingProgress.fillAmount <= 100)
        {
            loading_progress += Time.deltaTime * speedLoad;
            roundLoad = Mathf.RoundToInt(loading_progress);
            m_LoadingProgress.fillAmount = 0 + loading_progress / 100;
        }

        if (roundLoad == 100)//fix bug with ~ 100 value
        {
            m_LoadingProgress.fillAmount = 1;
        }

        if (m_LoadingProgress.fillAmount == 1)
        {
            m_LoadingProgress.fillAmount = 0;
            loading_progress = 0;
            SpawnCoins();
            //m_Coins.SetActive(true); // spawn coins
            //CheckBuyingCharactersAutoGettingCoins();//auto getting
        }
    }

    void SpawnCoins()
    {
        GameObject coinsPrefab = (GameObject)Instantiate(m_Coins);

        coinsPrefab.transform.SetParent(GameObject.Find("CoffeeShop").transform);
        coinsPrefab.GetComponent<RectTransform>().localPosition = new Vector2(UnityEngine.Random.Range(-150f, 150f), UnityEngine.Random.Range(50, -50));
        coinsPrefab.GetComponent<RectTransform>().localScale = new Vector2(1, 1);
    }

    // void CheckBuyingCharactersAutoGettingCoins()
    // {
    //     if ((isBuyingCh_1 == 1) || (isBuyingCh_2 == 1) || (isBuyingCh_3 == 1) || (isBuyingCh_4 == 1)) // IF BUY CHARACTER - AUTO GETTING COINS
    //     {
    //         GetCoins(1);
    //     }
    // }

    public void GetCoinsCoffeeShop()
    {
        Vector3 target;
        target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        target.z = 0;
        GameObject coinsPrefab = (GameObject)Instantiate(m_CoinsCoffeeShop, target, Quaternion.identity);
        coinsPrefab.transform.SetParent(GameObject.Find("CoinsByTap").transform);
        coinsPrefab.GetComponent<RectTransform>().localScale = new Vector2(1, 1);
        GameManager.Instance.coins += coinsShop;

        GameManager.Instance.counterGlobalTaps++;
        PlayerPrefs.SetInt("COUNTER_GLOBAL_TAPS", GameManager.Instance.counterGlobalTaps);
    }

    public void GetCoins(float increaseCoins)
    {
        GameManager.Instance.coins = GameManager.Instance.coins + coinsShop * increaseCoins;
    }

    void CheckStagesCoffeeShop()
    {
        if (numberOfUpdate == 20)
        {
            buttonUpdateCoffeeShop.interactable = false;
        }
    }

    private void Update()
    {
        if (isPause == false)
        {
            StartGenerateCoins();
            CheckCoinsUpdate();
        }

        textCoins.text = GameManager.Instance.coins.ToString("f2");
        textHearts.text = GameManager.Instance.hearts.ToString();
        textButtonUpdateCoffeeShop.text = "Update for " + costUpdate.ToString("##,#");
        textStagesOfLevels.text = "Stage " + numberOfUpdate + " of 20";
        //nameCoffeeShopText.text = GameManager.Instance.nameCoffeeShop; text area in develop

        CheckStagesCoffeeShop();
        CheckLoadingProgressUpdateButton();
        SaveKeysCoffeeShop();
        GameManager.Instance.Save();
    }

    private IEnumerator CreateCarCoins_1(float waitTime)
    {
        if (isPause == false)
        {
            while (true)
            {
                yield return new WaitForSeconds(waitTime);
                Transform car = (Transform)Instantiate(carCoinsButton_1);

                car.SetParent(GameObject.Find("Cars").transform);
                car.GetComponent<RectTransform>().localPosition = new Vector2(1500, -630.5f);
                car.GetComponent<RectTransform>().localScale = new Vector2(2, 2);
            }
        }
    }

    private IEnumerator CreateCarCoins_2(float waitTime)
    {
        if (isPause == false)
        {
            while (true)
            {
                yield return new WaitForSeconds(waitTime);
                Transform car = (Transform)Instantiate(carCoinsButton_2);

                car.SetParent(GameObject.Find("Cars").transform);
                car.GetComponent<RectTransform>().localPosition = new Vector2(-1500f, -585f);
                car.GetComponent<RectTransform>().localScale = new Vector2(2, 2);
            }
        }
    }

    private IEnumerator CreateBallCoins(float waitTime)
    {
        if (isPause == false)
        {
            while (true)
            {
                yield return new WaitForSeconds(waitTime);
                Transform ball = (Transform)Instantiate(ballCoinsButton);

                ball.SetParent(GameObject.Find("Balls").transform);
                ball.GetComponent<RectTransform>().localPosition = new Vector2(Random.Range(-500, 500), -1500);
                ball.GetComponent<RectTransform>().localScale = new Vector2(2, 2);
            }
        }
    }

    private IEnumerator CreatePeopleCoins_1(float waitTime)
    {
        if (isPause == false)
        {
            while (true)
            {
                yield return new WaitForSeconds(waitTime);
                Transform people = (Transform)Instantiate(peopleCoinsButton_1);
                people.SetParent(GameObject.Find("People").transform);
                people.GetComponent<RectTransform>().localPosition = new Vector2(1200, -360f);
                people.GetComponent<RectTransform>().localScale = new Vector2(0.5f, 0.5f);
            }
        }
    }

    private IEnumerator CreatePeopleCoins_2(float waitTime)
    {
        if (isPause == false)
        {
            while (true)
            {
                yield return new WaitForSeconds(waitTime);
                Transform people = (Transform)Instantiate(peopleCoinsButton_2);
                people.SetParent(GameObject.Find("People").transform);
                people.GetComponent<RectTransform>().localPosition = new Vector2(-1200, -360f);
                people.GetComponent<RectTransform>().localScale = new Vector2(0.5f, 0.5f);
            }
        }
    }

    public void OpenPanelPause()
    {
        canvasPanelMenu.SetActive(true);
    }

    public void ClosePanelPause()
    {
        canvasPanelMenu.SetActive(false);
    }

    public void OpenPanelFlowers()
    {
        canvasPlants.SetActive(true);
    }

    public void OpenPanelCharacter()
    {
        canvasCharacter.SetActive(true);
    }

    public void OpenPanelTables()
    {
        canvasTables.SetActive(true);
    }

    public void ClosePanelFlowers()
    {
        canvasPlants.SetActive(false);
    }

    public void ClosePanelCharacter()
    {
        canvasCharacter.SetActive(false);
    }

    public void ClosePanelTables()
    {
        canvasTables.SetActive(false);
    }

    public void OpenPanelAchiev()
    {
        canvasAchiev.SetActive(true);
    }

    public void ClosePanelAchiev()
    {
        canvasAchiev.SetActive(false);
    }

    public void OpenPanelTasks()
    {
        canvasTasks.SetActive(true);
    }

    public void ClosePanelTasks()
    {
        canvasTasks.SetActive(false);
    }
}
