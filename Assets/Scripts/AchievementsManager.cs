﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementsManager : MonoBehaviour {

    [SerializeField]
    private int[] achievementsCoins;

    [SerializeField]
    private GameObject charTextObj;

    [HideInInspector]
    private int ach_100_taps = 0;// 0 - not complete, 1 - complete
    [HideInInspector]
    private int getCoins_ach_100_taps = 0;

    [HideInInspector]
    private int ach_500_taps = 0;
    [HideInInspector]
    private int getCoins_ach_500_taps = 0;

    [HideInInspector]
    private int ach_1000_taps = 0;
    [HideInInspector]
    private int getCoins_ach_1000_taps = 0;

    [SerializeField]
    private Button button_ach_100_taps;
    [SerializeField]
    private Button button_ach_500_taps;
    [SerializeField]
    private Button button_ach_1000_taps;

    void Start()
    {
        LoadAchievementsKeys();
    }

    void LoadAchievementsKeys()
    {
        ach_100_taps = PlayerPrefs.GetInt("ACHIEVEMENT_100_TAPS");
        getCoins_ach_100_taps = PlayerPrefs.GetInt("GET_COINS_ACHIEVEMENT_100_TAPS");

        ach_500_taps = PlayerPrefs.GetInt("ACHIEVEMENT_500_TAPS");
        getCoins_ach_500_taps = PlayerPrefs.GetInt("GET_COINS_ACHIEVEMENT_500_TAPS");

        ach_1000_taps = PlayerPrefs.GetInt("ACHIEVEMENT_1000_TAPS");
        getCoins_ach_1000_taps = PlayerPrefs.GetInt("GET_COINS_ACHIEVEMENT_1000_TAPS");
    }

    void CheckGlobalTapsAchievements()//check global taps
    {
        if (GameManager.Instance.counterGlobalTaps >= 100 && getCoins_ach_100_taps == 0)
        {
            PlayerPrefs.SetInt("ACHIEVEMENT_100_TAPS", 1);
            button_ach_100_taps.interactable = true;
            charTextObj.SetActive(true);
        }

        if (getCoins_ach_100_taps == 1)
        {
            button_ach_100_taps.interactable = false;
        }

        if (GameManager.Instance.counterGlobalTaps >= 500 && getCoins_ach_500_taps == 0)
        {
            PlayerPrefs.SetInt("ACHIEVEMENT_500_TAPS", 1);
            button_ach_500_taps.interactable = true;
            charTextObj.SetActive(true);
        }

        if (getCoins_ach_500_taps == 1)
        {
            button_ach_500_taps.interactable = false;
        }

        if (GameManager.Instance.counterGlobalTaps >= 1000 && getCoins_ach_1000_taps == 0)
        {
            PlayerPrefs.SetInt("ACHIEVEMENT_1000_TAPS", 1);
            button_ach_1000_taps.interactable = true;
            charTextObj.SetActive(true);
        }

        if (getCoins_ach_1000_taps == 1)
        {
            button_ach_1000_taps.interactable = false;
        }
    }

    public void GetCoinsAchievement_1()
    {
        GameManager.Instance.hearts += achievementsCoins[0];
        getCoins_ach_100_taps = 1;
        PlayerPrefs.SetInt("GET_COINS_ACHIEVEMENT_100_TAPS", 1);
        charTextObj.SetActive(false);
    }

    public void GetCoinsAchievement_2()
    {
        GameManager.Instance.hearts += achievementsCoins[1];
        getCoins_ach_500_taps = 1;
        PlayerPrefs.SetInt("GET_COINS_ACHIEVEMENT_500_TAPS", 1);
        charTextObj.SetActive(false);
    }

    public void GetCoinsAchievement_3()
    {
        GameManager.Instance.hearts += achievementsCoins[2];
        getCoins_ach_1000_taps = 1;
        PlayerPrefs.SetInt("GET_COINS_ACHIEVEMENT_1000_TAPS", 1);
        charTextObj.SetActive(false);
    }

    void Update()
    {
        CheckGlobalTapsAchievements();
    }
}
