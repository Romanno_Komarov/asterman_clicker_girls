﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecipesShopManager : MonoBehaviour
{
    [SerializeField]
    private Button buttonCoffee_1_coins;
    [SerializeField]
    private Button buttonCoffee_2_coins;
    [SerializeField]
    private Button buttonCoffee_3_coins;
    [SerializeField]
    private Button buttonCoffee_4_coins;
    [SerializeField]
    private Button buttonCoffee_5_coins;

    [SerializeField]
    private Button buttonCoffee_1_hearts;
    [SerializeField]
    private Button buttonCoffee_2_hearts;
    [SerializeField]
    private Button buttonCoffee_3_hearts;
    [SerializeField]
    private Button buttonCoffee_4_hearts;
    [SerializeField]
    private Button buttonCoffee_5_hearts;

    private int isBuyingCoffee_1 = 0;
    private int isBuyingCoffee_2 = 0;
    private int isBuyingCoffee_3 = 0;
    private int isBuyingCoffee_4 = 0;
    private int isBuyingCoffee_5 = 0;

    void LoadCoffeeKeys()
    {
        isBuyingCoffee_1 = PlayerPrefs.GetInt("IS_BUYING_COFFEE_1");
        isBuyingCoffee_2 = PlayerPrefs.GetInt("IS_BUYING_COFFEE_2");
        isBuyingCoffee_3 = PlayerPrefs.GetInt("IS_BUYING_COFFEE_3");
        isBuyingCoffee_4 = PlayerPrefs.GetInt("IS_BUYING_COFFEE_4");
        isBuyingCoffee_5 = PlayerPrefs.GetInt("IS_BUYING_COFFEE_5");
    }

    void Start()
    {
        LoadCoffeeKeys();
        CheckBuyingFlowers();
    }

    void CheckBuyingFlowers()
    {
        if (isBuyingCoffee_1 == 1)
        {
            buttonCoffee_1_coins.interactable = false;
            buttonCoffee_1_hearts.interactable = false;
        }

        if (isBuyingCoffee_2 == 1)
        {
            buttonCoffee_2_coins.interactable = false;
            buttonCoffee_2_hearts.interactable = false;
        }

        if (isBuyingCoffee_3 == 1)
        {
            buttonCoffee_3_coins.interactable = false;
            buttonCoffee_3_hearts.interactable = false;
        }

        if (isBuyingCoffee_4 == 1)
        {
            buttonCoffee_4_coins.interactable = false;
            buttonCoffee_4_hearts.interactable = false;
        }

        if (isBuyingCoffee_5 == 1)
        {
            buttonCoffee_5_coins.interactable = false;
            buttonCoffee_5_hearts.interactable = false;
        }
    }

    public void BuyCoffee_1_Hearts(int heartsPrice)
    {
        if (GameManager.Instance.hearts >= heartsPrice)
        {
            GameManager.Instance.hearts -= heartsPrice;
            buttonCoffee_1_coins.interactable = false;
            buttonCoffee_1_hearts.interactable = false;
            CoffeeShop_1_Manager.Instance.coinsShop = CoffeeShop_1_Manager.Instance.coinsShop * (100 + 5) / 100;
            PlayerPrefs.SetInt("IS_BUYING_COFFEE_1", 1);
        }
    }

    public void BuyCoffee_2_Hearts(int heartsPrice)//all coffee for 1 hearts
    {
        if (GameManager.Instance.hearts >= heartsPrice)
        {
            GameManager.Instance.hearts -= heartsPrice;
            buttonCoffee_2_coins.interactable = false;
            buttonCoffee_2_hearts.interactable = false;
            CoffeeShop_1_Manager.Instance.coinsShop = CoffeeShop_1_Manager.Instance.coinsShop * (100 + 5) / 100;
            PlayerPrefs.SetInt("IS_BUYING_COFFEE_2", 1);
        }
    }

    public void BuyCoffee_3_Hearts(int heartsPrice)//all coffee for 1 hearts
    {
        if (GameManager.Instance.hearts >= heartsPrice)
        {
            GameManager.Instance.hearts -= heartsPrice;
            buttonCoffee_3_coins.interactable = false;
            buttonCoffee_3_hearts.interactable = false;
            CoffeeShop_1_Manager.Instance.coinsShop = CoffeeShop_1_Manager.Instance.coinsShop * (100 + 5) / 100;
            PlayerPrefs.SetInt("IS_BUYING_COFFEE_3", 1);
        }
    }

    public void BuyCoffee_4_Hearts(int heartsPrice)//all coffee for 1 hearts
    {
        if (GameManager.Instance.hearts >= heartsPrice)
        {
            GameManager.Instance.hearts -= heartsPrice;
            buttonCoffee_4_coins.interactable = false;
            buttonCoffee_4_hearts.interactable = false;
            CoffeeShop_1_Manager.Instance.coinsShop = CoffeeShop_1_Manager.Instance.coinsShop * (100 + 5) / 100;
            PlayerPrefs.SetInt("IS_BUYING_COFFEE_4", 1);
        }
    }

    public void BuyCoffee_5_Hearts(int heartsPrice)//all coffee for 1 hearts
    {
        if (GameManager.Instance.hearts >= heartsPrice)
        {
            GameManager.Instance.hearts -= heartsPrice;
            buttonCoffee_5_coins.interactable = false;
            buttonCoffee_5_hearts.interactable = false;
            CoffeeShop_1_Manager.Instance.coinsShop = CoffeeShop_1_Manager.Instance.coinsShop * (100 + 5) / 100;
            PlayerPrefs.SetInt("IS_BUYING_COFFEE_5", 1);
        }
    }

    public void BuyCoffee_1_Coins(int price)
    {
        if (GameManager.Instance.coins >= price)
        {
            GameManager.Instance.coins -= price;
            buttonCoffee_1_coins.interactable = false;
            buttonCoffee_1_hearts.interactable = false;
            CoffeeShop_1_Manager.Instance.coinsShop = CoffeeShop_1_Manager.Instance.coinsShop * (100 + 5) / 100;
            PlayerPrefs.SetInt("IS_BUYING_COFFEE_1", 1);
        }
    }

    public void BuyCoffee_2_Coins(int price)
    {
        if (GameManager.Instance.coins >= price)
        {
            GameManager.Instance.coins -= price;
            buttonCoffee_2_coins.interactable = false;
            buttonCoffee_2_hearts.interactable = false;
            CoffeeShop_1_Manager.Instance.coinsShop = CoffeeShop_1_Manager.Instance.coinsShop * (100 + 5) / 100;
            PlayerPrefs.SetInt("IS_BUYING_COFFEE_2", 1);
        }
    }

    public void BuyCoffee_3_Coins(int price)
    {
        if (GameManager.Instance.coins >= price)
        {
            GameManager.Instance.coins -= price;
            buttonCoffee_3_coins.interactable = false;
            buttonCoffee_3_hearts.interactable = false;
            CoffeeShop_1_Manager.Instance.coinsShop = CoffeeShop_1_Manager.Instance.coinsShop * (100 + 5) / 100;
            PlayerPrefs.SetInt("IS_BUYING_COFFEE_3", 1);
        }
    }

    public void BuyCoffee_4_Coins(int price)
    {
        if (GameManager.Instance.coins >= price)
        {
            GameManager.Instance.coins -= price;
            buttonCoffee_4_coins.interactable = false;
            buttonCoffee_4_hearts.interactable = false;
            CoffeeShop_1_Manager.Instance.coinsShop = CoffeeShop_1_Manager.Instance.coinsShop * (100 + 5) / 100;
            PlayerPrefs.SetInt("IS_BUYING_COFFEE_4", 1);
        }
    }

    public void BuyCoffee_5_Coins(int price)
    {
        if (GameManager.Instance.coins >= price)
        {
            GameManager.Instance.coins -= price;
            buttonCoffee_5_coins.interactable = false;
            buttonCoffee_5_hearts.interactable = false;
            CoffeeShop_1_Manager.Instance.coinsShop = CoffeeShop_1_Manager.Instance.coinsShop * (100 + 5) / 100;
            PlayerPrefs.SetInt("IS_BUYING_COFFEE_5", 1);
        }
    }
}
