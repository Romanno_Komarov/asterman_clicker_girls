﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    [HideInInspector]
    public float coins;

    [HideInInspector]
    public int hearts;

    [HideInInspector]
    public string namePlayer;

    [HideInInspector]
    public string nameCoffeeShop;

    [HideInInspector]
    public int isFirstPlaying = 0; // 0 - first playing, 1 - not first playing

    [HideInInspector]
    public int buyedLocalIsland_1 = 0; // 0 - not buyed, 1 - buyed

    [HideInInspector]
    public int buyedCoffeeShop_1 = 0; // 0 - not buyed, 1 - buyed

    [HideInInspector]
    public int isEnterNameCoffeeShop = 0;//0 - dont enter, 1 - enter yet

    [HideInInspector]
    public int counterTapsCars = 0;

    [HideInInspector]
    public int counterTapsBalls = 0;

    [HideInInspector]
    public int counterTapsPeople = 0;

    [HideInInspector]
    public int counterGlobalTaps = 0;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        Load();
    }

    public void Load()
    {
        coins = PlayerPrefs.GetFloat("COINS", 0);
        hearts = PlayerPrefs.GetInt("HEARTS", 1);

        namePlayer = PlayerPrefs.GetString("NAME_PLAYER");

        isFirstPlaying = PlayerPrefs.GetInt("IS_FIRST_PLAYING");

        //buyedLocalIsland_1 = PlayerPrefs.GetInt("IS_BUYED_LOCAL_ISLAND_1");
        //buyedCoffeeShop_1 = PlayerPrefs.GetInt("IS_BUYED_COFFEE_SHOP_1");
        
        //isEnterNameCoffeeShop = PlayerPrefs.GetInt("IS_ENTER_NAME_COFFEE_SHOP");
        //nameCoffeeShop = PlayerPrefs.GetString("NAME_COFFEE_SHOP");

        counterTapsCars = PlayerPrefs.GetInt("COUNTER_TAPS_CARS");
        counterTapsBalls = PlayerPrefs.GetInt("COUNTER_TAPS_BALLS");
        counterTapsPeople = PlayerPrefs.GetInt("COUNTER_TAPS_PEOPLE");

        counterGlobalTaps = PlayerPrefs.GetInt("COUNTER_GLOBAL_TAPS");
    }

    public void Save()
    {
        PlayerPrefs.SetFloat("COINS", coins);
        PlayerPrefs.SetInt("HEARTS", hearts);
    }
}
