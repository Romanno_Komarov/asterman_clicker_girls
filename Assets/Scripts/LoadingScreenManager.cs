﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingScreenManager : MonoBehaviour {

    void Start()
    {
        StartCoroutine(LoadScene());  
    }

    IEnumerator LoadScene()
    {
        yield return new WaitForSeconds(1.25f);

        if (GameManager.Instance.isFirstPlaying == 1)
        {
            SceneManager.LoadSceneAsync("CoffeeShop_1", LoadSceneMode.Single);
        }

        if (GameManager.Instance.isFirstPlaying == 0)
        {
            SceneManager.LoadSceneAsync("CreateName", LoadSceneMode.Single);
        }
    }
}
