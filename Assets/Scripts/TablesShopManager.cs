﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TablesShopManager : MonoBehaviour
{
    [SerializeField]
    private GameObject table_1;
    [SerializeField]
    private Button buttonTable_1_coins;
    [SerializeField]
    private Button buttonTable_1_hearts;

    [SerializeField]
    private GameObject board_1;
    [SerializeField]
    private Button buttonBoard_1_coins;
    [SerializeField]
    private Button buttonBoard_1_hearts;

    private int isBuyingTable_1 = 0;
    private int isBuyingBoard_1 = 0;

    void Start()
    {
        LoadKeys();
        CheckBuyingTablesBoards();
    }

    void LoadKeys()
    {
        isBuyingTable_1 = PlayerPrefs.GetInt("IS_BUYING_TABLE_1");
        isBuyingBoard_1 = PlayerPrefs.GetInt("IS_BUYING_BOARD_1");
    }

    void CheckBuyingTablesBoards()
    {
        if (isBuyingTable_1 == 1)
        {
            table_1.SetActive(true);
            buttonTable_1_coins.interactable = false;
            buttonTable_1_hearts.interactable = false;
        }

        if (isBuyingBoard_1 == 1)
        {
            board_1.SetActive(true);
            buttonBoard_1_coins.interactable = false;
            buttonBoard_1_hearts.interactable = false;
        }
    }

    public void BuyTable_1_Coins(int price)
    {
        if (GameManager.Instance.coins >= price)
        {
            GameManager.Instance.coins -= price;
            table_1.SetActive(true);
            buttonTable_1_coins.interactable = false;
            buttonTable_1_hearts.interactable = false;
            PlayerPrefs.SetInt("IS_BUYING_TABLE_1", 1);
        }
    }

    public void BuyBoard_1_Coins(int price)
    {
        if (GameManager.Instance.coins >= price)
        {
            GameManager.Instance.coins -= price;
            board_1.SetActive(true);
            buttonBoard_1_coins.interactable = false;
            buttonBoard_1_hearts.interactable = false;
            PlayerPrefs.SetInt("IS_BUYING_BOARD_1", 1);
        }
    }

    public void BuyTable_1_Hearts(int price)
    {
        if (GameManager.Instance.hearts >= price)
        {
            GameManager.Instance.hearts -= price;
            table_1.SetActive(true);
            buttonTable_1_coins.interactable = false;
            buttonTable_1_hearts.interactable = false;
            CoffeeShop_1_Manager.Instance.coinsShop = CoffeeShop_1_Manager.Instance.coinsShop * (100 + 10) / 100;
            PlayerPrefs.SetInt("IS_BUYING_TABLE_1", 1);
        }
    }

    public void BuyBoard_1_Hearts(int price)
    {
        if (GameManager.Instance.hearts >= price)
        {
            GameManager.Instance.hearts -= price;
            board_1.SetActive(true);
            buttonBoard_1_coins.interactable = false;
            buttonBoard_1_hearts.interactable = false;
            CoffeeShop_1_Manager.Instance.coinsShop = CoffeeShop_1_Manager.Instance.coinsShop * (100 + 5) / 100;
            PlayerPrefs.SetInt("IS_BUYING_BOARD_1", 1);
        }
    }
}
