﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinsButtonBehavior : MonoBehaviour {

    [SerializeField]
    private Button button;

    void Awake()
    {
        button.onClick.AddListener(OnButtonClicked);
    }

    void OnDestroy()
    {
        button.onClick.RemoveListener(OnButtonClicked);
    }

    void OnButtonClicked()
    {
        CoffeeShop_1_Manager.Instance.GetCoins(1);
        Destroy(this.gameObject);
    }

    void Start()
    {
        StartCoroutine(AutoGettingCoins());
    }

    IEnumerator AutoGettingCoins()
    {
        yield return new WaitForSeconds(3.5f);
        CoffeeShop_1_Manager.Instance.GetCoins(1);
        Destroy(this.gameObject);
    }
}
