﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallCoinsBehavior : MonoBehaviour
{

    [SerializeField]
    private Button button;

    [SerializeField]
    private Transform ballButton;

    [SerializeField]
    private Sprite ballSprite_1;

    [SerializeField]
    private Sprite ballSprite_2;

    [SerializeField]
    private float speedBall;

    void Awake()
    {
        button.onClick.AddListener(OnButtonClicked);
    }

    void OnDestroy()
    {
        button.onClick.RemoveListener(OnButtonClicked);
    }

    void Start()
    {
        int randomNumber = Random.Range(1, 3);

        if (randomNumber == 1)
        {
            button.image.sprite = ballSprite_1;
        }

        if (randomNumber == 2)
        {
            button.image.sprite = ballSprite_2;
        }
    }

    void OnButtonClicked()
    {
        CoffeeShop_1_Manager.Instance.GetCoinsCoffeeShop();
        CoffeeShop_1_Manager.Instance.GetCoins(1.5f);//x1.5 coins for tap ball

        GameManager.Instance.counterTapsBalls++;
        Destroy(this.gameObject);
    }

    void SaveCountersTapsCars()
    {
        PlayerPrefs.SetInt("COUNTER_TAPS_BALLS", GameManager.Instance.counterTapsBalls);
    }

    void MovingBall()
    {
        if (ballButton.transform.localPosition.y < 1800)
        {
            ballButton.Translate(Vector3.up * Time.deltaTime * speedBall);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void Update()
    {
        SaveCountersTapsCars();
    }

    void FixedUpdate()
    {
        MovingBall();
    }
}
