﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopManager : MonoBehaviour
{
    [SerializeField]
    private Text textCoins;

    [SerializeField]
    private Text textHearts;

    public void BuyCoins(int countCoins)
    {
        GameManager.Instance.coins += countCoins;
        PlayerPrefs.SetFloat("COINS", GameManager.Instance.coins);
    }

    public void BuyHearts(int countHearts)
    {
        GameManager.Instance.hearts += countHearts;
        PlayerPrefs.SetFloat("HEARTS", GameManager.Instance.hearts);
    }

    void Update()
    {
        textCoins.text = GameManager.Instance.coins.ToString("f2");
        textHearts.text = GameManager.Instance.hearts.ToString();
    }
}
