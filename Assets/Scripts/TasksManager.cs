﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TasksManager : MonoBehaviour
{
    [SerializeField]
    private int[] tasksCoins;

    [SerializeField]
    private GameObject charTextObj;

    [HideInInspector]
    private int task_cars = 0;// 0 - not complete, 1 - complete
    [HideInInspector]
    private int getCoins_task_cars = 0;

    [HideInInspector]
    private int task_balls = 0;
    [HideInInspector]
    private int getCoins_task_balls = 0;

    [HideInInspector]
    private int task_people = 0;
    [HideInInspector]
    private int getCoins_task_people = 0;

    [SerializeField]
    private Button buttonCarsTask;
    [SerializeField]
    private Button buttonBallsTask;
    [SerializeField]
    private Button buttonPeopleTask;

    void Start()
    {
        LoadTasksKeys();
    }

    void LoadTasksKeys()
    {
        task_cars = PlayerPrefs.GetInt("TASK_CARS");
        getCoins_task_cars = PlayerPrefs.GetInt("GET_COINS_TASK_CARS");

        task_balls = PlayerPrefs.GetInt("TASK_BALLS");
        getCoins_task_balls = PlayerPrefs.GetInt("GET_COINS_TASK_BALLS");

        task_people = PlayerPrefs.GetInt("TASK_PEOPLE");
        getCoins_task_people = PlayerPrefs.GetInt("GET_COINS_TASK_PEOPLE");
    }

    void CheckCarsTask()//check cars
    {
        if (GameManager.Instance.counterTapsCars >= 100 && getCoins_task_cars == 0)
        {
            PlayerPrefs.SetInt("TASK_CARS", 1);
            buttonCarsTask.interactable = true;
            charTextObj.SetActive(true);
        }

        if (getCoins_task_cars == 1)
        {
            buttonCarsTask.interactable = false;
        }
    }

    void CheckBallsTask()//check balls
    {
        if (GameManager.Instance.counterTapsBalls >= 10 && getCoins_task_balls == 0)
        {
            PlayerPrefs.SetInt("TASK_BALLS", 1);
            buttonBallsTask.interactable = true;
            charTextObj.SetActive(true);
        }

        if (getCoins_task_balls == 1)
        {
            buttonBallsTask.interactable = false;
        }
    }

    void CheckPeopleTask()//check people
    {
        if (GameManager.Instance.counterTapsPeople >= 50 && getCoins_task_people == 0)
        {
            PlayerPrefs.SetInt("TASK_PEOPLE", 1);
            buttonPeopleTask.interactable = true;
            charTextObj.SetActive(true);
        }

        if (getCoins_task_people == 1)
        {
            buttonPeopleTask.interactable = false;
        }
    }

    public void GetCoinsForCarsTask()
    {
        GameManager.Instance.coins += tasksCoins[0];
        getCoins_task_cars = 1;
        PlayerPrefs.SetInt("GET_COINS_TASK_CARS", 1);
        charTextObj.SetActive(false);
    }

    public void GetCoinsForBallsTask()
    {
        GameManager.Instance.coins += tasksCoins[1];
        getCoins_task_balls = 1;
        PlayerPrefs.SetInt("GET_COINS_TASK_BALLS", 1);
        charTextObj.SetActive(false);
    }

    public void GetCoinsForPeopleTask()
    {
        GameManager.Instance.coins += tasksCoins[2];
        getCoins_task_people = 1;
        PlayerPrefs.SetInt("GET_COINS_TASK_PEOPLE", 1);
        charTextObj.SetActive(false);
    }

    void Update()
    {
        CheckCarsTask();
        CheckBallsTask();
        CheckPeopleTask();
    }
}
