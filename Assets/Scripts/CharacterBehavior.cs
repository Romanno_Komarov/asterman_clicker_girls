﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBehavior : MonoBehaviour {

    public float speed;

    public bool IsLeft { get; set; }
    public bool IsRight { get; set; }


    private float m_moveTime = 0.0f;
    private bool m_isMoveRight;

    public void Tick()
    {
        if (m_moveTime <= Time.time)
        {
            if (m_isMoveRight)
            {
                IsLeft = true;
                IsRight = false;
                //transform.GetComponent<SpriteRenderer>().flipX = true;
            }
            else
            {
                IsLeft = false;
                IsRight = true;
                //transform.GetComponent<SpriteRenderer>().flipX = false;
            }

            m_moveTime = Time.time + 4;
            m_isMoveRight = !m_isMoveRight;
        }
    }

    void Update()
    {
        Tick();

        if (IsRight)
        {
            transform.Translate(Vector3.right * Time.deltaTime * speed);
        }

        else

            if (IsLeft)
            {
                transform.Translate(Vector3.left * Time.deltaTime * speed);
            }
    }
}
