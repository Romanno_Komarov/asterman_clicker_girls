﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LocalMapManager : MonoBehaviour
{
    [SerializeField]
    private Text textCoins;

    public int[] priceCoffeeShops;

    public Text textCoffeeShop_1;

    private void Start()
    {
        GameManager.Instance.Load();
        textCoins.text = GameManager.Instance.coins + " coins";

        if (GameManager.Instance.buyedCoffeeShop_1 == 0)
        {
            textCoffeeShop_1.text = priceCoffeeShops[0] + " coins";
        }
        else
        {
            textCoffeeShop_1.text = "Enter";
        }
    }

    public void BuyTripButton(int numberCoffeeShop)
    {
        if (numberCoffeeShop == 1 && GameManager.Instance.buyedCoffeeShop_1 == 0)
        {
            if (GameManager.Instance.coins >= priceCoffeeShops[0])
            {
                GameManager.Instance.coins -= priceCoffeeShops[0];
                PlayerPrefs.SetInt("IS_BUYED_COFFEE_SHOP_1", 1);
                SceneManager.LoadSceneAsync("CoffeeShop_" + numberCoffeeShop);
                GameManager.Instance.Save();
            }
        }

        if (numberCoffeeShop == 1 && GameManager.Instance.buyedCoffeeShop_1 == 1)
        {
            SceneManager.LoadSceneAsync("CoffeeShop_" + numberCoffeeShop);
        }
    }
}
