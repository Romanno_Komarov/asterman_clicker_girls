﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GlobalMapManager : MonoBehaviour
{
    [SerializeField]
    private Text textHello = null;

    [SerializeField]
    private Text textCoins = null;

    private void Start()
    {
        GameManager.Instance.Load();
        textHello.text = "Hello, " + GameManager.Instance.namePlayer + " ! This is a global map, choose an island and go on a trip!";
        textCoins.text = GameManager.Instance.coins + " coins";
    }

    public void BuyTripButton(int numberLocalIsland)
    {
        if (numberLocalIsland == 1)
        {
            PlayerPrefs.SetInt("IS_BUYED_LOCAL_ISLAND_1", 1);
            GameManager.Instance.Save();
            SceneManager.LoadSceneAsync("LocalMap_Island_" + numberLocalIsland);
        }
    }
}
