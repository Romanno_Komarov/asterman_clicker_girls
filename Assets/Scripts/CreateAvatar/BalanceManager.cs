﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalanceManager : MonoBehaviour
{
    static public BalanceManager Instanse;

    [SerializeField]
    private EyesInfo[] eyes;

    [SerializeField]
    private NoseInfo[] nose;

    [SerializeField]
    private HairsInfo[] hairs;

    [SerializeField]
    private MouthInfo[] mouth;

    [SerializeField]
    private ClothesInfo[] clothes;

    public EyesInfo[] Eyes { get { return eyes; } }

    public NoseInfo[] Nose { get { return nose; } }

    public HairsInfo[] Hairs { get { return hairs; } }

    public MouthInfo[] Mouth { get { return mouth; } }

    public ClothesInfo[] Clothes { get { return clothes; } }

    void Awake()
    {
        Instanse = this;
    }
}
