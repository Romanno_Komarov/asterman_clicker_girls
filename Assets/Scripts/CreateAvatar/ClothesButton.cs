﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClothesButton : MonoBehaviour
{
    [SerializeField]
    private ClothesType clothesType;

    [SerializeField]
    private Button button;

    void Awake()
    {
        button.onClick.AddListener(OnButtonClicked);
    }

    void OnDestroy()
    {
        button.onClick.RemoveListener(OnButtonClicked);
    }

    void OnButtonClicked()
    {
        WardrobeManager.Instanse.SetClothes(clothesType);
    }

}
