﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Wardrobe : MonoBehaviour
{
    static public Wardrobe Instance;

    [SerializeField]
    private Image avatarNose;

    [SerializeField]
    private Image avatarEyes;

    [SerializeField]
    private Image avatarHairs;

    [SerializeField]
    private Image avatarMouth;

    [SerializeField]
    private Image avatarClothes;

    private EyesType currentEyesType;
    private EyesType eyesType;

    private NoseType currentNoseType;
    private NoseType noseType;

    private HairsType currentHairsType;
    private HairsType hairsType;

    private MouthType currentMouthType;
    private MouthType mouthType;

    private ClothesType currentClothesType;
    private ClothesType clothesType;

    private List<EyesType> boughtEyes = new List<EyesType>();
    private List<NoseType> boughtNose = new List<NoseType>();
    private List<HairsType> boughtHairs = new List<HairsType>();
    private List<MouthType> boughtMouth = new List<MouthType>();
    private List<ClothesType> boughtClothes = new List<ClothesType>();

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        eyesType = (EyesType)PlayerPrefs.GetInt("WARDROBE_EYES");//eyes
        noseType = (NoseType)PlayerPrefs.GetInt("WARDROBE_NOSE");//nose
        hairsType = (HairsType)PlayerPrefs.GetInt("WARDROBE_HAIRS");//hairs
        mouthType = (MouthType)PlayerPrefs.GetInt("WARDROBE_MOUTH");//mouth
        clothesType = (ClothesType)PlayerPrefs.GetInt("WARDROBE_CLOTHES");//clothes

        SetEyes(eyesType);
        SetNose(noseType);
        SetHairs(hairsType);
        SetMouth(mouthType);
        SetClothes(clothesType);

        int boughtEyesCount = PlayerPrefs.GetInt("EYES_COUNT");
        int boughtNoseCount = PlayerPrefs.GetInt("NOSE_COUNT");
        int boughtHairsCount = PlayerPrefs.GetInt("HAIRS_COUNT");
        int boughtMouthCount = PlayerPrefs.GetInt("MOUTH_COUNT");
        int boughtClothesCount = PlayerPrefs.GetInt("CLOTHES_COUNT");

        for (int i = 0; i < boughtEyesCount; i++)
        {
            EyesType bouthEyesType = (EyesType)PlayerPrefs.GetInt("BOUTHG_EYES" + i);
            boughtEyes.Add(bouthEyesType);
        }

        for (int i = 0; i < boughtNoseCount; i++)
        {
            NoseType bouthNoseType = (NoseType)PlayerPrefs.GetInt("BOUTHG_NOSE" + i);
            boughtNose.Add(bouthNoseType);
        }

        for (int i = 0; i < boughtHairsCount; i++)
        {
            HairsType bouthHairsType = (HairsType)PlayerPrefs.GetInt("BOUTHG_HAIRS" + i);
            boughtHairs.Add(bouthHairsType);
        }

        for (int i = 0; i < boughtMouthCount; i++)
        {
            MouthType bouthMouthType = (MouthType)PlayerPrefs.GetInt("BOUTHG_MOUTH" + i);
            boughtMouth.Add(bouthMouthType);
        }

        for (int i = 0; i < boughtClothesCount; i++)
        {
            ClothesType bouthClothesType = (ClothesType)PlayerPrefs.GetInt("BOUTHG_CLOTHES" + i);
            boughtClothes.Add(bouthClothesType);
        }
    }

    public void SetNose(NoseType noseType)
    {
        Object obj = Resources.Load("Wardrobe/Acs/" + noseType.ToString());
        Texture2D texture = (Texture2D)Resources.Load("Wardrobe/Acs/" + noseType.ToString());
        Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        avatarNose.sprite = sprite;
        currentNoseType = noseType;
    }

    public void SetEyes(EyesType eyesType)
    {
        Object obj = Resources.Load("Wardrobe/Eyes/" + eyesType.ToString());
        Texture2D texture = (Texture2D)Resources.Load("Wardrobe/Eyes/" + eyesType.ToString());
        Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        avatarEyes.sprite = sprite;
        currentEyesType = eyesType;
    }

    public void SetHairs(HairsType hairsType)
    {
        Object obj = Resources.Load("Wardrobe/Hairs/" + hairsType.ToString());
        Texture2D texture = (Texture2D)Resources.Load("Wardrobe/Hairs/" + hairsType.ToString());
        Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        avatarHairs.sprite = sprite;
        currentHairsType = hairsType;
    }

    public void SetMouth(MouthType mouthType)
    {
        Object obj = Resources.Load("Wardrobe/Mouth/" + mouthType.ToString());
        Texture2D texture = (Texture2D)Resources.Load("Wardrobe/Mouth/" + mouthType.ToString());
        Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        avatarMouth.sprite = sprite;
        currentMouthType = mouthType;
    }

    public void SetClothes(ClothesType clothesType)
    {
        Object obj = Resources.Load("Wardrobe/Clothes/" + clothesType.ToString());
        Texture2D texture = (Texture2D)Resources.Load("Wardrobe/Clothes/" + clothesType.ToString());
        Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));
        avatarClothes.sprite = sprite;
        currentClothesType = clothesType;
    }

    public void Save()
    {
        PlayerPrefs.SetInt("WARDROBE_EYES", (int)eyesType);
        PlayerPrefs.SetInt("EYES_COUNT", boughtEyes.Count);

        PlayerPrefs.SetInt("WARDROBE_NOSE", (int)noseType);
        PlayerPrefs.SetInt("NOSE_COUNT", boughtNose.Count);

        PlayerPrefs.SetInt("WARDROBE_HAIRS", (int)hairsType);
        PlayerPrefs.SetInt("HAIRS_COUNT", boughtHairs.Count);

        PlayerPrefs.SetInt("WARDROBE_MOUTH", (int)mouthType);
        PlayerPrefs.SetInt("MOUTH_COUNT", boughtMouth.Count);

        PlayerPrefs.SetInt("WARDROBE_CLOTHES", (int)clothesType);
        PlayerPrefs.SetInt("CLOTHES_COUNT", boughtClothes.Count);

        for (int i = 0; i < boughtEyes.Count; i++)
        {
            PlayerPrefs.SetInt("BOUTHG_EYES" + i, (int)boughtEyes[i]);
        }

        for (int i = 0; i < boughtNose.Count; i++)
        {
            PlayerPrefs.SetInt("BOUTHG_NOSE" + i, (int)boughtNose[i]);
        }

        for (int i = 0; i < boughtHairs.Count; i++)
        {
            PlayerPrefs.SetInt("BOUTHG_HAIRS" + i, (int)boughtHairs[i]);
        }

        for (int i = 0; i < boughtMouth.Count; i++)
        {
            PlayerPrefs.SetInt("BOUTHG_MOUTH" + i, (int)boughtMouth[i]);
        }

        for (int i = 0; i < boughtClothes.Count; i++)
        {
            PlayerPrefs.SetInt("BOUTHG_CLOTHES" + i, (int)boughtClothes[i]);
        }
    }

    public void Buy()
    {
        eyesType = currentEyesType;
        noseType = currentNoseType;
        hairsType = currentHairsType;
        mouthType = currentMouthType;
        clothesType = currentClothesType;

        if (eyesType != EyesType.Default)
        {
            EyesInfo eyesInfo = BalanceManager.Instanse.Eyes.First(h => h.eyesType == currentEyesType);
            boughtEyes.Add(currentEyesType);
        }

        if (noseType != NoseType.Default)
        {
            NoseInfo skinInfo = BalanceManager.Instanse.Nose.First(s => s.noseType == currentNoseType);
            boughtNose.Add(currentNoseType);
        }

        if (hairsType != HairsType.Default)
        {
            HairsInfo hairsInfo = BalanceManager.Instanse.Hairs.First(a => a.hairsType == currentHairsType);
            boughtHairs.Add(currentHairsType);
        }

        if (mouthType != MouthType.Default)
        {
            MouthInfo mouthInfo = BalanceManager.Instanse.Mouth.First(m => m.mouthType == currentMouthType);
            boughtMouth.Add(currentMouthType);
        }

        if (clothesType != ClothesType.Default)
        {
            ClothesInfo clothesInfo = BalanceManager.Instanse.Clothes.First(c => c.clothesType == currentClothesType);
            boughtClothes.Add(currentClothesType);
        }
    }
}
