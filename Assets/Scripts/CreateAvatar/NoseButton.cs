﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoseButton : MonoBehaviour
{
    [SerializeField]
    private NoseType noseType;

    [SerializeField]
    private Button button;

    void Awake()
    {
        button.onClick.AddListener(OnButtonClicked);
    }

    void OnDestroy()
    {
        button.onClick.RemoveListener(OnButtonClicked);
    }

    void OnButtonClicked()
    {
        WardrobeManager.Instanse.SetNose(noseType);
    }

}
