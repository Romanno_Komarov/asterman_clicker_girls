﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WardrobeManager : MonoBehaviour
{
    public static WardrobeManager Instanse;

    public GameObject[] scrollView;

    [SerializeField]
    private Wardrobe wardrobe; 

    void Awake()
    {
        Instanse = this;
    }

    public void Save()
    {
        wardrobe.Save();
    }

    public void Buy()
    {
        wardrobe.Buy();
    }

    public void SetEyes(EyesType eyesType)
    {
        wardrobe.SetEyes(eyesType);
    }

    public void SetNose(NoseType noseType)
    {
        wardrobe.SetNose(noseType);
    }

    public void SetHairs(HairsType hairsType)
    {
        wardrobe.SetHairs(hairsType);
    }

    public void SetMouth(MouthType mouthType)
    {
        wardrobe.SetMouth(mouthType);
    }

    public void SetClothes(ClothesType clothesType)
    {
        wardrobe.SetClothes(clothesType);
    }

    public void OpenScrollView(GameObject scrollView)
    {
        SetNameScrollView();
        scrollView.SetActive(true);
    }

    void SetNameScrollView()
    {
        if (scrollView[0].gameObject.activeSelf == true)
        {
            scrollView[1].gameObject.SetActive(false);
            scrollView[2].gameObject.SetActive(false);
            scrollView[3].gameObject.SetActive(false);
            scrollView[4].gameObject.SetActive(false);
        }

        if (scrollView[1].gameObject.activeSelf == true)
        {
            scrollView[0].gameObject.SetActive(false);
            scrollView[2].gameObject.SetActive(false);
            scrollView[3].gameObject.SetActive(false);
            scrollView[4].gameObject.SetActive(false);
        }

        if (scrollView[2].gameObject.activeSelf == true)
        {
            scrollView[0].gameObject.SetActive(false);
            scrollView[1].gameObject.SetActive(false);
            scrollView[3].gameObject.SetActive(false);
            scrollView[4].gameObject.SetActive(false);
        }

        if (scrollView[3].gameObject.activeSelf == true)
        {
            scrollView[0].gameObject.SetActive(false);
            scrollView[1].gameObject.SetActive(false);
            scrollView[2].gameObject.SetActive(false);
            scrollView[4].gameObject.SetActive(false);
        }

        if (scrollView[4].gameObject.activeSelf == true)
        {
            scrollView[0].gameObject.SetActive(false);
            scrollView[1].gameObject.SetActive(false);
            scrollView[3].gameObject.SetActive(false);
            scrollView[2].gameObject.SetActive(false);
        }
    }
}
